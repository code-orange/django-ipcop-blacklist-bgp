from datetime import timedelta
from ipaddress import IPv4Address
from sys import stdout
from time import sleep

from django.conf import settings
from django.core.management.base import BaseCommand

from django_ipcop_models.django_ipcop_models.models import *


class Command(BaseCommand):
    help = "Run this command from exabgp config"

    def handle(self, *args, **options):
        self.stdout.write("# " + self.help)

        # Keep the script running so ExaBGP doesn't stop running and tear down the BGP peering
        while True:
            self.announce_blacklist()
            sleep(10)

    def announce_blacklist(self, *args, **options):
        last_24h = datetime.now() - timedelta(hours=24)
        local_as = settings.IPCOP_LOCAL_AS

        current_abusers_ip4 = (
            IpcopAbuseIp4.objects.filter(report_received__gte=last_24h)
            .values_list("ip", flat=True)
            .distinct()
        )

        current_abusers_ip6 = (
            IpcopAbuseIp6.objects.filter(report_received__gte=last_24h)
            .values_list("ip", flat=True)
            .distinct()
        )

        # handle IPv4 blacklist
        for current_abuser_ip4 in current_abusers_ip4:
            current_ip = IPv4Address(
                IpcopKnownIp4.objects.get(id=current_abuser_ip4).ipaddr
            )
            # Print the command to STDIN so ExaBGP can execute
            stdout.write(
                "announce route "
                + str(current_ip)
                + "/32 "
                + "next-hop 192.0.2.1 "
                + "community [no-export "
                + str(local_as)
                + ":667]\n"
            )
            stdout.flush()
